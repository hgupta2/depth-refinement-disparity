# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 12:37:06 2019

@author: aadishesha
"""

import os
import cv2
import numpy as np
from matplotlib import pyplot as plt
from disparitycalc import get_disp
from utils import *
from lister import listcreator
#begin with basic location definitions
image_dir = r'K:\zz-dataset\segTrackAssets' #location of all jpg files (includes persp and ortho)
groundtruth_dir =r'K:\Demo\MLCameraMatch\Automation\Renders\7-10-19'
gtfolders=os.listdir(groundtruth_dir)
gtfolders=[thing for thing in gtfolders if 'Depth' in thing]
imgfolders=os.listdir(image_dir)
imgfolders=[thing for thing in imgfolders if 'Projection' not in thing ]
imgfolders=[thang for thang in imgfolders if 'Depth' not in thang ]
imgfolders=[thangs for thangs in imgfolders if 'Ortho' not in thangs ]
imgfolders.sort()
out_dir=r'K:\zz-dataset\disparities2'
import re
##################################################################################

c=0
for item in imgfolders:
    imagelist=os.listdir(os.path.join(image_dir,item))
    if len(imagelist)<9:
        continue
    else:
        combinations=listcreator(imagelist)
        for comb in combinations:
            c+=1
            L=comb[0]
            R=comb[1]
            al=str(int(re.findall('\d+', L)[0]))
            ar=str(int(re.findall('\d+', R)[0]))
            dd='_disp_'+ al+'_'+ar
            imgL=(os.path.join(image_dir,item,L))
            imgR=(os.path.join(image_dir,item,R))
            alp=re.findall('\d+', L)[0]
            outfile=(os.path.join(out_dir,L)).replace(alp,dd)
            #print(imgL)
            imL=cv2.imread(imgL)
            imR=cv2.imread(imgR)
            ##imR=exr2png(imgR)
    #        imL = np.clip(imL,0,1)
    #        imL = (255*(imL**(1/2.4))).astype('uint8')
    #        imR = np.clip(imR,0,1)
    #        imR = (255*(imR**(1/2.4))).astype('uint8')
            #imL = np.transpose(imL, (1,0,2))[::-1,:,:]
            imL = cv2.cvtColor(imL, cv2.COLOR_BGR2GRAY)
            #imR = np.transpose(imR, (1,0,2))[::-1,:,:]
            imR = cv2.cvtColor(imR, cv2.COLOR_BGR2GRAY)
            disp= get_disp(imL,imR)
            #print (disp.shape)
    
            cv2.imwrite(outfile,disp)
            print (c,"/21816")

#    for image in imagelist:
#        imagepath=os.path.join(image_dir,item,image)
#        #image=cv2.imread(imagepath,-1)
#        image=exr2png(imagepath)[:,:,:3]
#        
#        break
#    break
    
