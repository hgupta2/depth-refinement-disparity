# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 12:51:05 2019

@author: aadishesha
"""

#!/usr/bin/env python
# coding: utf-8

# In[1]:


import cv2 
import numpy as np
import matplotlib.pyplot as plt
# root = "K:/zz-dataset/hdpersp2ortho"
# root = "C:/Users/hgupta/Desktop/code/segmentation_alpha"
root = 'C:/Users/hgupta/Desktop/code/segmentation_alpha/images4'



def get_threshIdx(disp):
    hist,bins = np.histogram(disp.ravel(),256,[0,256])
    i_s = 0
    i_e = 255
    i_m = (int)((i_s + i_e) / 2) 

    w_l = np.sum(hist[:i_m+1])
    w_r = np.sum(hist[i_m+1:])
    while (i_s <= i_e):
        if (w_r > w_l):
            w_r = w_r - hist[i_e];
            i_e = i_e - 1
            if (((i_s + i_e) / 2) < i_m):
                w_r += hist[i_m];
                w_l = w_l - hist[i_m]
                i_m -= 1

        elif (w_l >= w_r):
            w_l = w_l - hist[i_s];
            i_s = i_s + 1
            if (((i_s + i_e) / 2) >= i_m):
                w_l += hist[i_m + 1]
                w_r -= hist[i_m + 1]
                i_m += 1
    return i_m



def get_threshIdx_inp(inp):

    hist,bins = np.histogram(inp.ravel(),256,[0,256])
    i_s = 0
    i_e = inp.max()

    i_m = (int)((i_s + i_e) / 2) 

    w_l = np.sum(hist[:i_m+1])
    w_r = np.sum(hist[i_m+1:])
    while (i_s <= i_e):
        if (w_r > w_l):
            w_r = w_r - hist[i_e];
            i_e = i_e - 1
            if (((i_s + i_e) / 2) < i_m):
                w_r += hist[i_m];
                w_l = w_l - hist[i_m]
                i_m -= 1

        elif (w_l >= w_r):
            w_l = w_l - hist[i_s];
            i_s = i_s + 1
            if (((i_s + i_e) / 2) >= i_m):
                w_l += hist[i_m + 1]
                w_r -= hist[i_m + 1]
                i_m += 1
    return i_m


def get_disp(img_grayL, img_grayR):
    window_size = 3                   # wsize default 3; 5; 7 for SGBM reduced size image; 15 for SGBM full size image (1300px and above); 5 Works nicely
 
    left_matcher = cv2.StereoSGBM_create(
        minDisparity=0,
        numDisparities=256,             # max_disp has to be dividable by 16 f. E. HH 192, 256
        blockSize=3,
        P1=8 * 3 * window_size ** 2,    # wsize default 3; 5; 7 for SGBM reduced size image; 15 for SGBM full size image (1300px and above); 5 Works nicely
        P2=32 * 3 * window_size ** 2,
        disp12MaxDiff=1,
        uniquenessRatio=15,
        speckleWindowSize=0,
        speckleRange=2,
        preFilterCap=63,
        mode=cv2.STEREO_SGBM_MODE_SGBM_3WAY
    )

    right_matcher = cv2.ximgproc.createRightMatcher(left_matcher)
    # FILTER Parameters
    lmbda = 80000
    sigma = 1.2
    visual_multiplier = 1.0

    wls_filter = cv2.ximgproc.createDisparityWLSFilter(matcher_left=left_matcher)
    wls_filter.setLambda(lmbda)
    wls_filter.setSigmaColor(sigma)

    displ = left_matcher.compute(img_grayL, img_grayR)
    dispr = right_matcher.compute(img_grayR, img_grayL)
    displ = np.int16(displ)
    dispr = np.int16(dispr)

    filteredImg = wls_filter.filter(displ, img_grayL, None, dispr)  # important to put "imgL" here!!!

    filteredImg = cv2.normalize(src=filteredImg, dst=filteredImg, beta=0, alpha=255, norm_type=cv2.NORM_MINMAX);
    filteredImg = np.uint8(filteredImg)
    return filteredImg

# In[ ]:


imgL = cv2.imread(root + '/09901269.jpg')
imgL = cv2.cvtColor(imgL, cv2.COLOR_BGR2GRAY)
imgL1=(imgL>50)*imgL

#
imgR = cv2.imread(root + '/09800044.jpg')
imgR = cv2.cvtColor(imgR, cv2.COLOR_BGR2GRAY)
imgR1=(imgR>50)*imgR
#
plt.imshow(imgL1, cmap = 'gray')
plt.show()
plt.imshow(imgR1, cmap = 'gray')
plt.show()
disp = get_disp(imgL1, imgR1)
#
#disp = np.where(disp > get_threshIdx(disp), disp, 0)
plt.imshow(disp, cmap = 'gray')
plt.colorbar()
plt.show()
cv2.imwrite(root + '/disp.jpg', disp)

hist = cv2.calcHist([disp],[0],None,[256],[0,256])
hist1=hist[1:]
slop=[]
for i in range(0,len(hist1)-5,5):
    slope=int(((hist1[i+5]-hist1[i])/5))
    slop.append(slope)

maxind=slop.index(max(slop[25:]))
actind=maxind
trind=actind*5
disp1=disp>trind
disp =disp1*disp
plt.imshow(disp,cmap='gray')
plt.show()
cv2.imwrite(root + '/final_disp.jpg', disp)


# imgL = cv2.imread(root + '/09901269_my_object.jpg')
# imgL = cv2.cvtColor(imgL, cv2.COLOR_BGR2GRAY)

# imgR = cv2.imread(root + '/09800044_my_object.jpg')
# imgR = cv2.cvtColor(imgR, cv2.COLOR_BGR2GRAY)

# # print(get_threshIdx_inp(imgL))

# # print(imgL.max(), len(np.unique(imgL)))

# imgL = np.where(imgL > get_threshIdx_inp(imgL), imgL, 0)
# imgR = np.where(imgR > get_threshIdx_inp(imgR), imgR, 0)

# plt.imshow(imgL, cmap = 'gray')
# # plt.show()
# plt.imshow(imgR, cmap = 'gray')
# # plt.show()
# disp = get_disp(imgL, imgR)

# plt.imshow(disp, cmap = 'gray')
# plt.colorbar()
# plt.show()

# cv2.imwrite('depth/disparity.jpg', disp)

# print(disp.max())
# print(get_threshIdx(disp))

# print(disp.max(), len(np.unique(disp)))

# disp = np.where(disp > get_threshIdx(disp), disp, 0)
# plt.imshow(disp, cmap = 'gray')
# plt.colorbar()
# plt.show()

